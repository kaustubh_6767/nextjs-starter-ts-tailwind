FROM node:18-alpine

# Additional Dependencies
RUN apk add bash --no-cache

# Create app directory
WORKDIR /app

# Install app dependencies
COPY package.json .
COPY package-lock.json .

RUN npm install

# Copy app source
COPY . .

# ENVIRONMENTS

ARG APP_ENV
ENV NEXT_PUBLIC_APP_ENV=$APP_ENV

ARG API_BASE_URL
ENV NEXT_PUBLIC_API_BASE_URL=$API_BASE_URL

ARG APP_BASE_URL
ENV NEXT_PUBLIC_APP_BASE_URL=$APP_BASE_URL

ARG BUILD_TAG
ENV NEXT_PUBLIC_BUILD_TAG=$BUILD_TAG

# Compile application
RUN npm run build

ENTRYPOINT ["/bin/bash", "-c", "npm run start"]

# Server startup PORT
ENV PORT 3000

EXPOSE ${PORT} 3000
