import { forgotPasswordDto } from '@/server/auth/types/login-dto.interface';
import { resetRedux } from '../redux/actions';
import { HttpService } from './network/http.service';

export class AuthService extends HttpService {
  async performLogin(data: { email: string; password: string }) {
    try {
      const result = await this.post('/api/auth/login', {
        ...data,
      });
      const { access_token } = result;
      localStorage.setItem('access_token', access_token);
      return result;
    } catch (err: any) {
      if (err?.status && err?.status === 401) {
        throw new Error('Invalid credentials');
      } else {
        throw new Error(err?.message);
      }
    }
  }

  async performLogout() {
    try {
      await this.post('/api/auth/logout', {});
      localStorage.removeItem('access_token');
    } finally {
      resetRedux();
      document.cookie = '';
      if (typeof window !== 'undefined') window.location.href = `${window.location.origin}/auth/login`;
    }
  }

  async getLoggedInUser() {
    const result = await this.get('/api/auth/me', { silent: true });
    return result;
  }

  async refreshSession() {
    const result = await this.post('/api/auth/refresh', {}, { silent: true });
    const { access_token } = result;
    localStorage.setItem('access_token', access_token);
    return result;
  }

  async ssoAuth() {
    return this.get(`/api/v1/auth/login-sso?provider=OKTA&app=VBM`);
  }

  async forgotPassword(forgotPasswordPayload: forgotPasswordDto) {
    return this.post('/api/v1/auth/forgot-password/request', forgotPasswordPayload);
  }
}
