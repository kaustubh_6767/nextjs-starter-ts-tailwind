import axios, { AxiosError, AxiosInstance } from 'axios';
import Router from 'next/router';
import environments from '../../common/environments';
import logger from '../../common/logger';
import { showLoader } from '../../redux/actions';

type Configs = {
  ssrServer?: boolean;
};

export class ClientSideHttpClient {
  private axios: AxiosInstance;

  constructor(configs: Configs = { ssrServer: false }) {
    const { ssrServer } = configs;
    this.axios = axios.create({
      baseURL: !ssrServer ? environments.API_BASE_URL : environments.APP_BASE_URL || '/',
    });

    this.axios.interceptors.request.use(
      config => {
        if (!config.silent) {
          showLoader(true);
        }
        const token = localStorage.getItem('access_token');
        if (token) {
          // eslint-disable-next-line no-param-reassign
          config.headers = {
            ...config.headers,
            Authorization: `Bearer ${token}`,
          };
        }
        return config;
      },
      (error: AxiosError) => {
        showLoader(false);
        logger.error(error);
        return Promise.reject(error.toJSON());
      },
    );

    this.axios.interceptors.response.use(
      response => {
        showLoader(false);
        return response;
      },
      (error: AxiosError) => {
        showLoader(false);
        const path = Router.asPath;
        if (
          error.response?.status === 401 &&
          !path.includes('/auth/refresh') &&
          !path.includes('/i/') &&
          !path.includes('building-wellness-pulse')
        ) {
          Router.push(`/auth/refresh?redirectTo=${encodeURIComponent(path)}`);
        }
        return Promise.reject(error.toJSON());
      },
    );
  }

  get httpClient() {
    return this.axios;
  }
}
