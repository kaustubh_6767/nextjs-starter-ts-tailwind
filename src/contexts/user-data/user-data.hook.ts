import { useContext } from 'react';
import { UserDataContext } from './user-data.provider';

function useUserData() {
  const userData = useContext(UserDataContext);
  return userData;
}

export default useUserData;
