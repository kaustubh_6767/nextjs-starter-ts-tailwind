import environments from '@/common/environments';
import { useRouter } from 'next/router';
import ga from '@/lib/ga';
import React, { useEffect, useMemo } from 'react';
import useSWR from 'swr';
import { USER_ROLE } from '../../common/constants';
import SessionLoader from '../../components/session-loader/session-loader.component';
import { AuthService } from '../../services/auth.service';
import { User } from './user.types';

export interface UserDataContextInterface {
  user: User | null;
  isAdmin: boolean;
  roles: USER_ROLE[];
}

export const UserDataContext = React.createContext<UserDataContextInterface>({
  user: null,
  isAdmin: false,
  roles: [],
});

const UserDataProvider = ({ children }: any) => {
  const router = useRouter();
  const authService = new AuthService({ ssrServer: true });
  const { data: userData } = useSWR('getLoggedInUser', () => authService.getLoggedInUser());

  useEffect(() => {
    const handleRouteChange = (url: URL) => {
      const userParams: any = {};
      if (userData) {
        userParams.customer = userData.companyDetails[0]?.name || '';
        userParams.customerId = userData.companyIds[0] || '';
        userParams.role = userData.roleDetails[0]?.name || '';
      }
      ga.event({ action: 'page_view', page_path: url, ...userParams });
    };
    if (environments.GA_TRACKING) router.events.on('routeChangeComplete', handleRouteChange);
    return () => {
      if (environments.GA_TRACKING) router.events.off('routeChangeComplete', handleRouteChange);
    };
  }, [userData]);

  const contextValue: UserDataContextInterface = useMemo(
    () =>
      userData
        ? {
            user: userData,
            isAdmin: !!userData.roleDetails.find((role: any) => role.name === USER_ROLE.ADMIN),
            roles: userData.roleDetails.map((role: any) => role.name),
          }
        : { user: null, isAdmin: false, roles: [] },
    [userData],
  );

  return (
    <UserDataContext.Provider value={contextValue}>
      {contextValue.user ? children : <SessionLoader />}
    </UserDataContext.Provider>
  );
};

export default UserDataProvider;
