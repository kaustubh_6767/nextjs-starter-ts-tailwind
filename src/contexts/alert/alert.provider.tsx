import React, { useState, useMemo, useCallback } from 'react';

export enum AlertVariant {
  ERROR = 'error',
  INFO = 'info',
  SUCCESS = 'success',
  WARNING = 'warning',
}

export interface AlertContextInterface {
  variant: AlertVariant;
  message: string;
  showAlert: (message: string, variant?: AlertVariant) => void;
}

export const AlertContext = React.createContext<AlertContextInterface>({
  variant: AlertVariant.INFO,
  message: '',
  showAlert: () => {},
});

const AlertProvider = ({ children }: any) => {
  const [alert, setAlert] = useState({
    variant: AlertVariant.INFO,
    message: '',
  });

  const showAlert = useCallback((message, variant = AlertVariant.INFO) => {
    setAlert({ message, variant });
  }, []);

  const contextValue: AlertContextInterface = useMemo(
    () => ({
      message: alert.message,
      variant: alert.variant,
      showAlert,
    }),
    [alert, showAlert],
  );

  // TODO: Create Alert Component
  return <AlertContext.Provider value={contextValue}>{children}</AlertContext.Provider>;
};

export default AlertProvider;
