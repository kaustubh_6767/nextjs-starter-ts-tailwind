import { useContext } from 'react';
import { AlertContext } from './alert.provider';

function useAlert() {
  const { message, variant, showAlert } = useContext(AlertContext);
  return { message, variant, showAlert };
}

export default useAlert;
