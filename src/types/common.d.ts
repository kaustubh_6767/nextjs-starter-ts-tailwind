import { NextPage } from 'next';
import { ReactElement } from 'react';

export type REDUX_ACTION = {
  type: string;
  payload?: any;
};
export type Response<T = any> = {
  data: T;
  status: number;
};

export type PaginatedResponse<T = any> = {
  data: T;
  total: number;
  page: number;
  size: number;
};

export type SortByFilters<T = any> = {
  [key in keyof T]: 'asc' | 'desc';
};

export type PaginationArguments<T = any> = {
  pageSize: number | string;
  pageNo: number | string;
  sortBy?: SortByFilters<T>;
  /**
   * comma separated 1 / -1
   */
  sortValues?: string | number;
  /**
   * comma separated field names
   */
  sortFields?: string;
};

export type PaginationProps = PaginationArguments & {
  total: number;
};

export type NextPageWithLayout<P = {}> = NextPage<P> & {
  getLayout: (page: ReactElement | ReactElement[]) => ReactNode;
};
