import type { NextApiRequest } from 'next';
import { getAccessToken, getRefreshToken } from '../../../common/helpers';
import { AuthService } from '../services/auth.service';

export class AuthController {
  private authService: AuthService;

  constructor(accessToken?: string) {
    this.authService = new AuthService({ accessToken });
  }

  async login(req: NextApiRequest) {
    const reqBody = req.body;
    const result = await new AuthService().login(reqBody);
    return result;
  }

  async getLoggedInUser(req: NextApiRequest) {
    const accessToken = getAccessToken(req);
    const result = await this.authService.getLoggedInUser(accessToken);
    return result;
  }

  async refreshSession(req: NextApiRequest) {
    const refreshToken = getRefreshToken(req);
    const result = this.authService.refreshSession(refreshToken);
    return result;
  }
}
