import jwt from 'jsonwebtoken';
import { HttpService } from '../../../services/network/http.service';
import { forgotPasswordDto, ILoginDto, resetPasswordDto } from '../types/login-dto.interface';

export class AuthService extends HttpService {
  login(loginDto: ILoginDto) {
    const access_token = jwt.sign({ username: loginDto.username }, process.env.JWT_SECRET_KEY || 'nextjs-ts-starter');
    return {
      data: {
        access_token,
        refresh_token: '',
      },
      status: 200,
    };
  }

  async getLoggedInUser(accessToken: string) {
    const payload = jwt.verify(accessToken, process.env.JWT_SECRET_KEY || 'nextjs-ts-starter');
    return {
      data: payload,
      status: 200,
    };
  }

  async refreshSession(_refreshToken: string) {
    return {
      data: {
        access_token: '',
        refresh_token: '',
      },
      status: 200,
    };
  }

  async forgotPassword(_forgotPasswordPayload: forgotPasswordDto) {
    return { status: 200, message: 'OK' };
  }

  async resetPassword(_resetPasswordPayload: resetPasswordDto) {
    return { status: 200, message: 'OK' };
  }
}
