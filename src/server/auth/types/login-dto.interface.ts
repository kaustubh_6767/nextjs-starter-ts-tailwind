interface ILoginDto {
  username: string;
  password: string;
}
interface forgotPasswordDto {
  username: string;
}
interface resetPasswordDto {
  password: string;
  token: string;
}

export type { ILoginDto, forgotPasswordDto, resetPasswordDto };
