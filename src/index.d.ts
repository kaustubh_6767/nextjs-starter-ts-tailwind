import * as Axios from 'axios';

export type ThemeMode = 'dark' | 'light';

declare module 'axios' {
  export interface AxiosRequestConfig<_D = any> extends Axios.AxiosRequestConfig {
    silent?: boolean;
  }
}

declare global {
  type GTagEvent = {
    action: string;
    [key: string]: any;
  };
  interface Window {
    gtag: (context: 'config' | 'event' | 'set', ...params: any) => void;
  }
}
