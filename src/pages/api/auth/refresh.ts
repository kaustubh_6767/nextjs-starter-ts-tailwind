import type { NextApiRequest, NextApiResponse } from 'next';
import jwt from 'jsonwebtoken';
import { generateCookie, getAccessToken, getRefreshToken } from '../../../common/helpers';
import { AuthController } from '../../../server/auth/controllers/auth.controller';
import logger from '../../../common/logger';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const { method, cookies } = req;
  const { persist_session } = cookies;
  const rememberMe = persist_session === 'true';
  const authController = new AuthController(getAccessToken(req as any));
  switch (method) {
    case 'POST':
      try {
        const refreshToken = getRefreshToken(req);
        if (!refreshToken) {
          res.setHeader('Clear-Site-Data', `"cookies", "storage"`);
          res.redirect(307, '/auth/login');
          return;
        }
        const result = await authController.refreshSession(req);
        const { access_token, refresh_token } = result.data;
        const accessTokenPayload = jwt.decode(access_token, { json: true }) || {};
        const accessTokenData = {
          name: 'access_token',
          value: access_token,
          expires: new Date((accessTokenPayload.exp as number) * 1000).toISOString(),
          maxAge: (accessTokenPayload.exp as number) - Math.floor(Date.now() / 1000),
          path: '/',
        };
        const accessTokenCookie = generateCookie(accessTokenData, { persist: rememberMe });
        const refreshTokenPayload = jwt.decode(refresh_token, { json: true }) || {};
        const refreshTokenData = {
          name: 'refresh_token',
          value: refresh_token,
          expires: new Date((refreshTokenPayload.exp as number) * 1000).toISOString(),
          maxAge: (refreshTokenPayload.exp as number) - Math.floor(Date.now() / 1000),
          path: '/api/auth/refresh',
        };
        const refreshTokenCookie = generateCookie(refreshTokenData, { persist: rememberMe });
        const rememberMeCookie = generateCookie(
          {
            ...refreshTokenData,
            name: 'persist_session',
            value: `${rememberMe}`,
            path: '/',
          },
          { persist: rememberMe },
        );
        res.setHeader('Set-Cookie', [accessTokenCookie, refreshTokenCookie, rememberMeCookie]);
        res.status(result.status).json(result.data);
      } catch (error) {
        logger.error('Error while refreshing session', error);
        const { status = 500, message = 'Internal Server Error' } = (error || {}) as any;
        res.setHeader('Clear-Site-Data', `"cookies", "storage"`);
        res.status(status).json({
          statusCode: status,
          message,
        });
      }
      break;
    default:
      res.setHeader('Allow', ['POST']);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
}
