import type { NextApiRequest, NextApiResponse } from 'next';
import { generateCookie } from '../../../common/helpers';
import logger from '../../../common/logger';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const { method } = req;
  switch (method) {
    default:
      try {
        const accessTokenData = {
          name: 'access_token',
          value: '',
          expires: new Date(Date.now() - 1000 * 60).toISOString(),
          maxAge: -1000 * 60,
          path: '/',
        };
        const accessTokenCookie = generateCookie(accessTokenData);
        const refreshTokenData = {
          name: 'refresh_token',
          value: '',
          expires: new Date(Date.now() - 1000 * 60).toISOString(),
          maxAge: -1000 * 60,
          path: '/api/auth/refresh',
        };
        const refreshTokenCookie = generateCookie(refreshTokenData);
        const rememberMeCookie = generateCookie({
          ...refreshTokenData,
          name: 'persist_session',
          value: '',
          path: '/',
        });
        res.setHeader('Set-Cookie', [accessTokenCookie, refreshTokenCookie, rememberMeCookie]);
        res.status(200).json({
          message: 'Logged out successfully',
        });
      } catch (error) {
        logger.error(error);
        const { status = 500, message = 'Internal Server Error' } = (error || {}) as any;
        res.status(status).json({
          statusCode: status,
          message,
        });
      }
      break;
  }
}
