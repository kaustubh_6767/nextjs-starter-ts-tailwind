import type { NextApiRequest, NextApiResponse } from 'next';
import { getAccessToken } from '../../../common/helpers';
import logger from '../../../common/logger';
import { AuthController } from '../../../server/auth/controllers/auth.controller';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const { method } = req;
  const authController = new AuthController(getAccessToken(req as any));
  switch (method) {
    case 'GET':
      try {
        const result = await authController.getLoggedInUser(req);
        res.setHeader('Cache-Control', 'private, max-age=82800');
        res.setHeader('Vary', 'Cookie');
        res.status(result.status).json(result.data);
      } catch (error) {
        logger.error(error);
        const { status = 500, message = 'Internal Server Error' } = (error || {}) as any;
        res.status(status).json({
          statusCode: status,
          message,
        });
      }
      break;

    default:
      res.setHeader('Allow', ['GET']);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
}
