import '@/styles/globals.css';
import React, { useEffect, useState } from 'react';
import { Provider } from 'react-redux';
import Head from 'next/head';
import { SWRConfig } from 'swr';
import { AppProps } from 'next/app';
import NextNProgress from 'nextjs-progressbar';
import { CacheProvider, EmotionCache } from '@emotion/react';
import createEmotionCache from '@/common/emotion-cache';
import { useStore } from '@/redux/store';
import AlertProvider from '../contexts/alert/alert.provider';
import MainLayout from '../layouts/main-layout/main-layout';
import DataLoader from '../components/data-loader/data-loader.component';
import SessionLoader from '../components/session-loader/session-loader.component';
import { NextPageWithLayout } from '../types/common';

const clientSideEmotionCache = createEmotionCache();

interface Props extends AppProps {
  Component: NextPageWithLayout;
  emotionCache: EmotionCache;
}

const App = (props: Props) => {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;
  const getLayout = Component.getLayout || (page => page);
  const store = useStore(pageProps.initialReduxState);

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(false);
  }, []);

  return (
    <CacheProvider value={emotionCache}>
      <Provider store={store}>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
        </Head>
        <AlertProvider>
          {loading && <SessionLoader />}
          <SWRConfig
            value={{
              revalidateOnFocus: false,
            }}
          >
            <MainLayout>
              <NextNProgress color="#19889e" />
              {getLayout(<Component {...pageProps} />)}
              <DataLoader />
            </MainLayout>
          </SWRConfig>
        </AlertProvider>
      </Provider>
    </CacheProvider>
  );
};

export default App;
