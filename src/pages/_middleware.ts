import { NextRequest, NextResponse } from 'next/server';

export function middleware(_req: NextRequest) {
  // const url = req.nextUrl.clone();

  // const secureRoutes = ['/'];

  // if (
  //   (secureRoutes.includes(url.pathname) || secureRoutes.find(path => matchRoutePath(path, url.pathname))) &&
  //   !getAccessToken(req as any)
  // ) {
  //   // eslint-disable-next-line no-console
  //   logger.info(url.pathname, '-->', '/auth/login');
  //   return new Response('', {
  //     status: 307,
  //     headers: {
  //       Location: '/auth/login',
  //     },
  //   });
  // } else {
  //   // eslint-disable-next-line no-console
  //   logger.info(url.pathname, '-->', url.pathname);
  //   return NextResponse.next();
  // }
  return NextResponse.next();
}
