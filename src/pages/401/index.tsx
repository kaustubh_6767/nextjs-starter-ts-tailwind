import { NextPage } from 'next';

const NotFound: NextPage = () => {
  return (
    <div
      style={{
        height: 'calc(100vh - 80px - 64px)',
        textAlign: 'center',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <div>
        <h1
          style={{
            display: 'inline-block',
            borderRight: '1px solid rgba(0, 0, 0,.3)',
            margin: 0,
            marginRight: '20px',
            padding: '10px 23px 10px 0',
            fontSize: '24px',
            fontWeight: 500,
            verticalAlign: 'top',
          }}
        >
          401
        </h1>
        <div
          style={{
            display: 'inline-block',
            textAlign: 'left',
            lineHeight: '49px',
            height: '49px',
            verticalAlign: 'middle',
          }}
        >
          <h2 style={{ fontSize: '16px', fontWeight: 'normal', lineHeight: 'inherit', margin: 0, padding: 0 }}>
            Unauthorized
          </h2>
        </div>
      </div>
    </div>
  );
};

export default NotFound;
