import Router, { useRouter } from 'next/router';
import { useEffect } from 'react';
import Head from 'next/head';
import type { GetServerSideProps } from 'next';
import Logo from '@/assets/images/logo.svg';
import Image from 'next/image';
import styles from './auth.module.scss';
import { AuthService } from '../../../services/auth.service';
import SessionLoader from '../../../components/session-loader/session-loader.component';
import Login from '../components/login/login.component';
import { getAccessToken } from '../../../common/helpers';
import Footer from '../../../components/footer/footer.component';
import useAlert from '../../../contexts/alert/alert.hook';
import { AlertVariant } from '../../../contexts/alert/alert.provider';
import ForgotPassword from '../components/forgot-password/forgot-password.component';
import ResetPassword from '../components/reset-password/reset-password-component';

interface Props {
  action: 'login' | 'forgot-password' | 'reset-password' | 'refresh';
}

const Auth = (props: Props) => {
  const { action } = props;
  const router = useRouter();
  const { showAlert } = useAlert();
  const authService = new AuthService({ ssrServer: true });

  const AUTHROUTES = {
    login: Login,
    'forgot-password': ForgotPassword,
    'reset-password': ResetPassword,
    refresh: SessionLoader,
  };

  const AuthActiveComponent = AUTHROUTES[action] !== undefined ? AUTHROUTES[action] : AUTHROUTES.login;

  const refreshSession = async () => {
    try {
      await authService.refreshSession();
      if (typeof router.query.redirectTo === 'string') {
        Router.push(router.query.redirectTo);
      } else {
        router.push('/');
      }
    } catch (error) {
      const { message = 'Something went wrong!' } = error as any;
      showAlert(`${message}`, AlertVariant.ERROR);
      router.replace('/auth/login');
    }
  };

  useEffect(() => {
    if (action === 'refresh') {
      refreshSession();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [action]);

  if (action === 'refresh') {
    return <SessionLoader />;
  }

  return (
    <>
      <div className={styles.container}>
        <Head>
          <title>
            {/* eslint-disable-next-line no-nested-ternary */}
            {action === 'forgot-password'
              ? 'Forgot Password'
              : action === 'reset-password'
              ? 'Reset Password'
              : 'Login'}{' '}
            | NextJs TS Starter
          </title>
        </Head>
        <div className={styles.cardContainer}>
          <div className={styles.card}>
            <div className={styles.cardHeader}>
              <div className={styles.logo}>
                <Image src={Logo} alt="Logo" width={160} />
              </div>
              <span className={styles.appTitle}>NextJs TS Starter</span>
            </div>
            <div className={styles.content}>
              <AuthActiveComponent />
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async context => {
  const { params, req } = context;
  const { action } = params || {};
  if (['login', 'forgot-password'].includes(params?.action as string) && getAccessToken(req)) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
      props: { action },
    };
  }
  return {
    props: { action },
  };
};

export default Auth;
