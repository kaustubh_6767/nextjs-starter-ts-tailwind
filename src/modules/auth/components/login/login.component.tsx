/* eslint-disable @typescript-eslint/no-unused-vars */
import { useState } from 'react';
import { useRouter } from 'next/router';
import { useFormik } from 'formik';
import type { NextComponentType } from 'next';
import Link from 'next/link';
import * as yup from 'yup';
import useAlert from '../../../../contexts/alert/alert.hook';
import { AlertVariant } from '../../../../contexts/alert/alert.provider';
import { AuthService } from '../../../../services/auth.service';

import styles from './login.module.scss';

interface FormValues {
  email: string;
  password: string;
  rememberMe: boolean;
}

const loginDtoSchema = yup.object().shape({
  email: yup.string().required().email(),
  password: yup.string().required(),
});

const Login: NextComponentType = () => {
  const { showAlert } = useAlert();
  const router = useRouter();
  const loginService = new AuthService({ ssrServer: true });
  const [showPassword, setShowPassword] = useState(false);

  const { values, touched, errors, handleBlur, handleChange, handleSubmit, handleReset, setFieldValue } =
    useFormik<FormValues>({
      initialValues: {
        email: '',
        password: '',
        rememberMe: true,
      },
      validationSchema: loginDtoSchema,
      onSubmit: async (data: FormValues) => {
        try {
          const result = await loginService.performLogin(data);
          const { user } = result;
          router.push('/');
          showAlert(`Welcome ${user.firstName}!`, AlertVariant.SUCCESS);
        } catch (error) {
          const { message = 'Something went wrong!' } = error as any;
          showAlert(`${message}`, AlertVariant.ERROR);
        }
      },
    });

  return (
    <div className={styles.container}>
      <p className={styles.title}>Sign In</p>
      <div className={styles.formContainer}>
        <form onSubmit={handleSubmit} onReset={handleReset}>
          <div className={styles.formControl}>Username</div>
          <div className={styles.formControl}>Password</div>
          <div
            className={styles.formControl}
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <span className={styles.link}>
              <Link href="/auth/forgot-password">Forgot Password?</Link>
            </span>
          </div>
          <div className={styles.formAction}>
            <Link href="/home">SIGN IN</Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
