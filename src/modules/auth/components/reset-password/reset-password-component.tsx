import React from 'react';
import { NextComponentType } from 'next';
import Link from 'next/link';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { AlertVariant } from '@/contexts/alert/alert.provider';
import useAlert from '@/contexts/alert/alert.hook';
import { AuthService } from '@/server/auth/services/auth.service';
import { useRouter } from 'next/router';
import { resetPasswordDto } from '@/server/auth/types/login-dto.interface';
import styles from '../login/login.module.scss';

interface FormValues {
  password: string;
  confirmPassword: string;
}

const ResetPasswordDtoSchema = yup.object().shape({
  password: yup.string().required(),
  confirmPassword: yup
    .string()
    .required('confirm password is a required field')
    .oneOf([yup.ref('password'), null], 'passwords must match'),
});

const ResetPassword: NextComponentType = () => {
  const { showAlert } = useAlert();
  const router = useRouter();
  const loginService = new AuthService({ ssrServer: false });
  const [token, setToken] = React.useState<any>('');
  const query = router?.query;
  React.useEffect(() => {
    if (!query?.token) {
      showAlert(`Unauthorized!`, AlertVariant.ERROR);
      router.replace('/auth/forgot-password');
    } else {
      setToken(query.token);
    }
  }, []);

  const MessageHandler = (status = 500, msg = '') => {
    if (status === 200) {
      router.replace('/auth/login');
    } else {
      showAlert(`${msg}`, AlertVariant.ERROR);
      router.replace('/auth/forgot-password');
    }
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { values, touched, errors, handleBlur, handleChange, handleSubmit, handleReset } = useFormik<FormValues>({
    initialValues: {
      password: '',
      confirmPassword: '',
    },
    validationSchema: ResetPasswordDtoSchema,
    onSubmit: async (data: FormValues) => {
      try {
        const payload: resetPasswordDto = {
          password: data.password,
          token,
        };
        await loginService
          .resetPassword(payload)
          .then(resp => MessageHandler(resp?.status, resp?.message))
          .catch(err => MessageHandler(err?.status, err?.message));
      } catch (error: any) {
        MessageHandler(error?.status, error?.message);
      }
    },
  });

  return (
    <div className={styles.container}>
      <p className={styles.title}>Reset Password</p>
      <div className={styles.formContainer}>
        <form onSubmit={handleSubmit} onReset={handleReset}>
          <div className={styles.formControl}>Form Control</div>

          <div className={styles.formControl}>Form Control</div>

          <div
            className={styles.formControl}
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              alignItems: 'center',
            }}
          >
            <span className={styles.link}>
              <Link href="/auth/login">Back to Sign In</Link>
            </span>
          </div>
          <div className={styles.formAction}>Reset Password</div>
        </form>
      </div>
    </div>
  );
};

export default ResetPassword;
