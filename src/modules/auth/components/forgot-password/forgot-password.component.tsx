import React from 'react';
import { NextComponentType } from 'next';
import Link from 'next/link';
import { useFormik } from 'formik';
import * as yup from 'yup';
import useAlert from '@/contexts/alert/alert.hook';
import { AlertVariant } from '@/contexts/alert/alert.provider';
import { AuthService } from '@/services/auth.service';
import { useRouter } from 'next/router';
import styles from '../login/login.module.scss';

interface FormValues {
  username: string;
}

const ForgotPasswordDtoSchema = yup.object().shape({
  username: yup.string().required(),
});

const ForgotPassword: NextComponentType = () => {
  const { showAlert } = useAlert();
  const router = useRouter();
  const authService = new AuthService({ ssrServer: false });

  const { handleSubmit, handleReset } = useFormik<FormValues>({
    initialValues: {
      username: '',
    },
    validationSchema: ForgotPasswordDtoSchema,
    onSubmit: async (data: FormValues) => {
      try {
        await authService.forgotPassword(data).then(resp => {
          if (resp?.status === 200) {
            router.replace('/auth/login');
          }
        });
      } catch (error) {
        const { message = 'Something went wrong!' } = error as any;
        showAlert(`${message}`, AlertVariant.ERROR);
      }
    },
  });

  return (
    <div className={styles.container}>
      <p className={styles.title}>Reset Password</p>
      <div className={styles.formContainer}>
        <form onSubmit={handleSubmit} onReset={handleReset}>
          <div className={styles.formControl}>Username</div>
          <div
            className={styles.formControl}
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              alignItems: 'center',
            }}
          >
            <span className={styles.link}>
              <Link href="/auth/login">Back to Sign In</Link>
            </span>
          </div>
          <div className={styles.formAction}>Submit</div>
        </form>
      </div>
    </div>
  );
};

export default ForgotPassword;
