/* eslint-disable @typescript-eslint/default-param-last */
import { AGENT } from '@/common/constants';
import { REDUX_ACTION } from '@/types/common';
import { combineReducers } from 'redux';
import * as types from './types';

export interface ISessionState {
  access_token: string | null;
}

export interface ICommonState {
  loader: boolean;
  agent: AGENT;
}

export interface IRootState {
  common: ICommonState;
}

const COMMON_INITIAL_STATE = {
  loader: false,
  agent: AGENT.WEB,
};

const ROOT_INITIAL_STATE: IRootState = {
  common: COMMON_INITIAL_STATE,
};

const commonReducer = (state = COMMON_INITIAL_STATE, action: REDUX_ACTION) => {
  const { type, payload } = action;
  switch (type) {
    case types.TOGGLE_LOADER:
      return { ...state, loader: payload };
    case types.SET_AGENT:
      return { ...state, agent: payload };
    default:
      return state;
  }
};

const combine = {
  common: commonReducer,
};

const appReducer = combineReducers(combine);

const reducers = (state: IRootState = ROOT_INITIAL_STATE, action: REDUX_ACTION) => {
  if (action.type === types.RESET_REDUX) {
    return appReducer({ common: state?.common }, action);
  }

  return appReducer(state, action);
};

export default reducers;
