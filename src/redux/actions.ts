import { initializeStore } from '@/redux/store';
import { AGENT } from '../common/constants';
import * as types from './types';

const store = initializeStore();

export const showLoader = (payload: boolean) => {
  store.dispatch({
    type: types.TOGGLE_LOADER,
    payload,
  });
};

export const setAgent = (payload: AGENT) => {
  store.dispatch({
    type: types.SET_AGENT,
    payload,
  });
};

export const resetRedux = () => {
  store.dispatch({
    type: types.RESET_REDUX,
  });
};
