import type { NextPage } from 'next';
import Head from 'next/head';
import Footer from '../../components/footer/footer.component';
import Header from '../../components/header/header.component';
import UserDataProvider from '../../contexts/user-data/user-data.provider';
import styles from './internal-layout.module.scss';

type Props = {
  noInset?: boolean;
  noPadding?: boolean;
  noPaper?: boolean;
};

const InternalLayout: NextPage<Props> = props => {
  const { children, noInset, noPadding, noPaper } = props;

  return (
    <>
      <Head>
        <title>NextJs TS Starter</title>
      </Head>
      <div
        className={`${styles.container} ${noInset ? styles.noInset : ''} ${noPadding ? styles.noPadding : ''}`}
        key="container"
      >
        <UserDataProvider>
          <Header />
          {noPaper ? (
            <div className={`${styles.content} ${noInset ? styles.noInset : ''}  ${noPadding ? styles.noPadding : ''}`}>
              {children}
            </div>
          ) : (
            <div className={`${styles.content} ${noInset ? styles.noInset : ''}  ${noPadding ? styles.noPadding : ''}`}>
              {children}
            </div>
          )}
        </UserDataProvider>
      </div>
      <Footer key="footer" />
    </>
  );
};

export default InternalLayout;
