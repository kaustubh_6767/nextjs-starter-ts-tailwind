import React from 'react';
import { USER_ROLE } from '../../common/constants';
import useUserData from '../../contexts/user-data/user-data.hook';

type RoleGuardT = {
  visibleForRoles?: USER_ROLE | USER_ROLE[];
  inVisibleForRoles?: USER_ROLE | USER_ROLE[];
  children: JSX.Element;
  fallback?: JSX.Element | null;
};

const RoleGuard: React.FC<RoleGuardT> = ({ visibleForRoles, inVisibleForRoles, children, fallback = null }) => {
  const { roles = [] } = useUserData();
  const allUserRolesArray = Object.values(USER_ROLE);

  let hasAccess = false;

  if (visibleForRoles && Array.isArray(visibleForRoles)) {
    hasAccess = visibleForRoles.some(role => roles.includes(role));
  }

  if (visibleForRoles && typeof visibleForRoles === 'string') {
    hasAccess = roles.includes(visibleForRoles);
  }

  if (inVisibleForRoles && Array.isArray(inVisibleForRoles)) {
    const allowed = allUserRolesArray.filter(role => !inVisibleForRoles.includes(role));
    hasAccess = allowed.some(role => roles.includes(role));
  }

  if (inVisibleForRoles && typeof inVisibleForRoles === 'string') {
    const allowed = allUserRolesArray.filter(role => role !== inVisibleForRoles);
    hasAccess = allowed.some(role => roles.includes(role));
  }

  if (hasAccess) {
    return children;
  }

  return fallback;
};

export default RoleGuard;
