import { useEffect } from 'react';

const SessionLoader = () => {
  useEffect(() => {
    const interval = setInterval(() => window.location.reload(), 60 * 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <div
      style={{
        position: 'fixed',
        top: 0,
        left: 0,
        zIndex: 92,
        background: 'linear-gradient(to bottom, var(--primary-dark), var(--primary-light))',
        height: '100vh',
        width: '100vw',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
      }}
    >
      <span
        style={{
          marginTop: 'var(--xxs)',
          textTransform: 'uppercase',
          color: 'var(--primary-contrast-text)',
          fontWeight: 700,
          fontSize: 'var(--md)',
        }}
      >
        {' '}
        NextJs TS Starter <sup style={{ fontSize: '12px' }}>TM</sup>
      </span>
      <span
        style={{
          marginTop: 'var(--xxs)',
          color: 'var(--primary-contrast-text)',
          fontWeight: 500,
          fontSize: 'var(--xs)',
        }}
      >
        Loading...
      </span>
    </div>
  );
};

export default SessionLoader;
