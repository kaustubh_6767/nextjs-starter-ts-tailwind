import { ReactNode } from 'react';
import styles from './tab-panel.module.scss';

type TabPanelProps = {
  children?: ReactNode;
  index: number;
  value: number;
};

const defaultProps: Partial<TabPanelProps> = {
  children: null,
};

const TabPanel = (props: TabPanelProps) => {
  const { children = null, value, index, ...other } = props;

  return (
    <div hidden={value !== index} {...other}>
      {value === index && <div className={styles.tabPanelContent}>{children}</div>}
    </div>
  );
};

TabPanel.defaultProps = defaultProps;

export default TabPanel;
