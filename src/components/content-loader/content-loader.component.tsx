import React from 'react';
import styles from './content-loader.module.scss';

const ContentLoader = () => {
  return (
    <div className={styles.loader}>
      <div className={styles['loader__lds-ellipsis']}>
        <div />
        <div />
        <div />
        <div />
      </div>
    </div>
  );
};

export default ContentLoader;
