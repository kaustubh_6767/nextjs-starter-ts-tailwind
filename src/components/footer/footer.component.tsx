/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import styles from './footer.module.scss';
import environments from '../../common/environments';

const Footer = () => {
  return (
    <div className={styles.container}>
      <div>
        <div className={styles.gridBox}>
          <ul>
            <li>
              <a target="_blank" href="https://example.com/" rel="noopener noreferrer">
                example.com
              </a>
            </li>
            <li>
              <a target="_blank" rel="noopener noreferrer" href="https://example.com/terms">
                Terms
              </a>
            </li>
            <li>
              <a target="_blank" rel="noopener noreferrer" href="https://example.com/privacy">
                Privacy
              </a>
            </li>
            <li>
              <a target="_blank" rel="noopener noreferrer" href="https://example.com/help">
                Help
              </a>
            </li>
            <li>
              <a href="tel:++X.XXX.XXX.XXXX">Country: +X.XXX.XXX.XXXX</a>
            </li>
            <li>
              <a href="tel:++X.XXX.XXX.XXXX">International: +X.XXX.XXX.XXXX</a>
            </li>
            <li>
              <a href="mailto:support@example.com"> Email: support@example.com </a>
            </li>
          </ul>
        </div>
        <div className={styles.copyRightLabel}>
          © Copyright {new Date().getFullYear()} Example, Ltd.
          <p className={styles['build-tag']}>Version: {environments.BUILD_TAG || 'N/A'}</p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
