import { useMemo, useEffect, useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import Logo from '@/assets/images/logo.svg';
import styles from './header.module.scss';
import { NavLink } from './types';

const Header = () => {
  const useWidth = () => {
    const [width, setWidth] = useState(window.innerWidth);
    const handleResize = () => setWidth(window.innerWidth);

    useEffect(() => {
      window.addEventListener('resize', handleResize);
      return () => window.removeEventListener('resize', handleResize);
    }, [width]);
    return width;
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const windowWidth = useWidth();

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const navLinks: NavLink[] = useMemo(
    () => [
      {
        key: '1',
        href: `/home`,
        label: 'Home',
        show: true,
      },
    ],
    [],
  );

  return (
    <div className={styles.container}>
      <Link href="/" passHref>
        <div className={styles.logo}>
          <Image src={Logo} alt="Logo" />
        </div>
      </Link>
    </div>
  );
};

export default Header;
