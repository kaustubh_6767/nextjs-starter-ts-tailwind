export type NavLink = {
  key: string;
  href: string;
  label: string;
  isActive?: () => boolean;
  show: boolean | undefined;
};
