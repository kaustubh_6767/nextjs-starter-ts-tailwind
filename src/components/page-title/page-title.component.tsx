import Head from 'next/head';

const PageTitle = ({ title }: { title: string }) => {
  return (
    <Head>
      <title>{title} | NextJs TS Starter</title>
    </Head>
  );
};

export default PageTitle;
