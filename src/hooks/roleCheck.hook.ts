import useUserData from '@/contexts/user-data/user-data.hook';
import { USER_ROLE } from '@/common/constants';

const useRoleCheck = () => {
  const { roles = [] } = useUserData();

  const checkUserRole = (role: USER_ROLE) => {
    if (role && typeof role === 'string') {
      return roles.includes(role);
    }
    return false;
  };

  return checkUserRole;
};

export default useRoleCheck;
