export enum ENVIRONMENTS {
  LOCAL = 'local',
  DEV = 'development',
  QA = 'qa',
  STAGE = 'stage',
  PRODUCTION = 'production',
}

export enum AGENT {
  WEB = 'web',
  MOBILE = 'mobile',
}

export enum USER_ROLE {
  ADMIN = 'ADMIN',
}

export const DEFAULT_PAGE_SIZE = 10;
/**
 * 15 min * 60 sec * 1000 ms
 */
export const REFRESH_INTERVAL = 15 * 60 * 1000;
