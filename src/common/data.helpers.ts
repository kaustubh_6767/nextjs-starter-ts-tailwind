import _ from 'lodash';

export const objectDeepDiff = (target: any, reference: any) => {
  let diffObj: any = Array.isArray(reference) ? [] : {};
  if (_.isEmpty(target) || _.isEmpty(reference)) {
    if (target !== reference) {
      diffObj = target;
    }
    return diffObj;
  }
  Object.getOwnPropertyNames(reference).forEach((prop: string) => {
    if (typeof reference[prop] === 'object') {
      diffObj[prop] = objectDeepDiff(target[prop], reference[prop]);
      if ((Array.isArray(diffObj[prop]) && diffObj[prop].length === 1) || _.isEmpty(diffObj[prop])) {
        delete diffObj[prop];
      }
    } else if (target[prop] !== reference[prop]) {
      diffObj[prop] = target[prop];
    }
  });
  return diffObj;
};
