// eslint-disable-next-line max-classes-per-file
export class Exception<T> extends Error {
  details: T;

  constructor(message: string, details: T) {
    super(message);
    this.details = details;
  }
}

export class ForbiddenException extends Error {
  status = 403;
}
