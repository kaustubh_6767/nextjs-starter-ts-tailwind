import { IncomingMessage } from 'http';
import { NextApiRequestCookies } from 'next/dist/server/api-utils';

export const camelCaseToDashed = (str: string): string => {
  return str.replace(/[A-Z]/g, m => `-${m.toLowerCase()}`);
};

export const flattenObject = (obj: { [key: string]: any }, separator = '.', prefix = '') => {
  const result: { [key: string]: any } = {};
  Object.keys(obj).forEach(key => {
    if (typeof obj[key] === 'object' && !Array.isArray(obj[key]) && obj[key] !== null) {
      const temp = flattenObject(obj[key], separator, '');
      Object.keys(temp).forEach(keyN => {
        result[camelCaseToDashed(`${prefix}${prefix ? separator : ''}${key}${separator}${keyN}`)] = temp[keyN];
      });
    } else if (typeof obj[key] !== 'function') {
      result[camelCaseToDashed(`${prefix}${prefix ? separator : ''}${key}`)] = obj[key];
    }
  });
  return result;
};

export const matchRoutePath = (pattern: string, pathname: string) => {
  if (pattern.endsWith('*')) {
    return pathname.startsWith(pattern.replace('*', ''));
  }
  return pattern === pathname;
};

export const generateCookie = (
  payload: { name: string; value: string; expires: string; maxAge: number; path: string; domain?: string },
  options: {
    secure?: boolean;
    persist?: boolean;
    httpOnly?: boolean;
  } = {},
) => {
  const defaultOptions = { secure: false, persist: true, httpOnly: true };
  const { name, value, expires, maxAge, path = '/', domain } = payload;
  const { secure, persist, httpOnly } = { ...defaultOptions, ...options };
  const cookie = `${name}=${value};${persist ? `Expires=${expires};Max-Age=${maxAge};` : ''}Path=${path};${
    httpOnly ? 'HttpOnly;' : ''
  }${secure ? 'Secure;' : ''}${domain ? `Domain=${domain};` : ''}`;
  return cookie;
};

export const getAccessToken = (
  request: IncomingMessage & {
    cookies: NextApiRequestCookies;
  },
) => {
  return request.cookies.access_token || '';
};

export const getBearerToken = (
  request: IncomingMessage & {
    cookies: NextApiRequestCookies;
  },
) => {
  return (request.headers.authorization || '').split(' ')[1] || '';
};

export const getRefreshToken = (
  request: IncomingMessage & {
    cookies: NextApiRequestCookies;
  },
) => request.cookies.refresh_token || '';

export const isJson = (str: string) => {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};

export const percentage = (val: number, max: number, decimals?: number) => {
  if (typeof val !== 'number' || Number.isNaN(val)) return 0;
  if (val < 0) return 0;
  if (max < 0) return 0;
  return typeof decimals === 'number' ? parseFloat(((val * 100) / max).toFixed(decimals)) || 0 : (val * 100) / max || 0;
};

export const getRoundedNumber = (val: number) => (val % 10 > 0 ? Math.ceil(val / 10) * 10 : val);

export const getRoundedIntValue = (val: number) => Math.round(Number(val || 0));

export const isNullOrUndefined = (val: any) => {
  if (val === null || val === undefined) return true;
  return false;
};

export const isArrayEqual = (arr1: string[], arr2: string[]) => {
  if (!Array.isArray(arr1)) {
    return false;
  }
  if (!Array.isArray(arr2)) {
    return false;
  }
  if (arr1.length !== arr2.length) {
    return false;
  }
  return arr1.every(val => arr2.includes(val)) && arr2.every(val => arr1.includes(val));
};

export const isObjectEmpty = (obj: { [key: string]: any }) => {
  // eslint-disable-next-line no-restricted-syntax, guard-for-in, no-unreachable-loop
  for (const _ in obj) {
    return false;
  }
  return true;
};

export const getBooleanOrUndefined = (val: boolean | undefined | null) => {
  return typeof val === 'boolean' ? val : undefined;
};

export const checkAndTrimNumber = (value: number | undefined | null, decimals = 0) => {
  if (value === undefined || value === null) {
    return '-';
  }
  if (value === 0) {
    return '0';
  }
  if (typeof value === 'string' && Number.isNaN(parseFloat(value))) {
    return '-';
  }
  return parseFloat(parseFloat(`${value}`).toFixed(decimals)).toLocaleString();
};

export const convertToFahrenheit = (value: number | string | null | undefined): number | null =>
  value === null || value === undefined ? null : parseFloat(`${Number(value) * 1.8 + 32}`);

export const percentageChange = (a: number | undefined | null, b: number | undefined | null) => {
  if (b && a) {
    let value: any = (b / a) * 100 - 100;
    if (value === 0) {
      value = `No Change ${value.toFixed(0)}%`;
    }
    if (value > 0) {
      value = `Up ${value.toFixed(0)}%`;
    }
    if (value < 0) {
      value = `Down ${Math.abs(value).toFixed(0)}%`;
    }
    return value;
  } else {
    return null;
  }
};

export const isNumOrZero = (a: any) => {
  if (typeof a !== 'undefined' && a !== null) {
    return true;
  } else {
    return false;
  }
};

export const getSnapshotValue = (snapValue: number | undefined | null) => {
  if (snapValue === undefined || snapValue === null || snapValue < 0) {
    return 0;
  }
  return snapValue.toFixed(2);
};
