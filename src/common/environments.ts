import logger from './logger';

export type ENVIRONMENT = {
  API_BASE_URL: string;
  APP_BASE_URL: string;
  APP_ENV?: string;
  BUILD_TAG?: string;
  GA_TRACKING: string;
};

const APP_ENV = process.env.NEXT_PUBLIC_APP_ENV || 'local';
const API_BASE_URL = process.env.NEXT_PUBLIC_API_BASE_URL || 'https://api.example.com';
const APP_BASE_URL = process.env.NEXT_PUBLIC_APP_BASE_URL || 'https://example.view.com';
const BUILD_TAG = process.env.NEXT_PUBLIC_BUILD_TAG || '';
const GA_TRACKING = process.env.NEXT_PUBLIC_GA_TRACKING || '';

const environments: ENVIRONMENT = {
  API_BASE_URL,
  APP_BASE_URL,
  BUILD_TAG,
  GA_TRACKING,
  APP_ENV,
};

logger.info(environments);

export default environments;
