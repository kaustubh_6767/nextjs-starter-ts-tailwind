#!/usr/bin/env bash

# put SSH key in `.ssh and make it accessible
mkdir -p ~/.ssh
echo "$SSH_PRIVATE_KEY_TOOLKIT" >~/.ssh/id_rsa
chmod 0600 ~/.ssh/id_rsa
echo "StrictHostKeyChecking no " >/root/.ssh/config

# configure git to use email of commit-owner
git config --global http.sslVerify false
git config --global pull.ff only
git config --global user.email "$CI_BOT_EMAIL"
git config --global user.name "$CI_BOT_NAME"
echo "setting origin remote to 'git@$CI_SERVER_HOST:$CI_PROJECT_PATH.git'"

# first cleanup any existing named remotes called 'origin' before re-setting the url
git remote rm origin
git remote add origin git@$CI_SERVER_HOST:$CI_PROJECT_PATH.git

# have the gitlab runner checkout be linked to the branch we are building
git checkout -B "$CI_BUILD_REF_NAME"
