export SONAR_TOKEN=$SONAR_SBP_TOKEN
export SONAR_HOST_URL=$SONAR_HOST_URL

VERSION=$(cat package.json |
  grep version |
  head -1 |
  awk -F: '{ print $2 }' |
  sed 's/[",]//g' | xargs)
echo "Version: $VERSION"

echo "sonar.projectVersion=$VERSION" >>sonar-project.properties

if [ $CI_PIPELINE_SOURCE == "push" ]; then
  echo "sonar.branch.name=$CI_COMMIT_BRANCH" >>sonar-project.properties
fi
