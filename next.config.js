/** @type {import('next').NextConfig} */
const path = require('path');
const withPWA = require('next-pwa')({
  dest: 'public',
  register: true,
  skipWaiting: true,
});

const nextConfig = {
  reactStrictMode: true,
  sassOptions: {
    includePaths: [path.join(__dirname, 'src')],
    prependData: `@import "~@/styles/resources.scss";`,
    crossOrigin: 'anonymous',
    swcMinify: true,
    optimizeFonts: true,
    reactStrictMode: true,
    generateEtags: false,
    poweredByHeader: false,
  },
  publicRuntimeConfig: {
    API_BASE_URL: process.env.API_BASE_URL,
    APP_BASE_URL: process.env.APP_BASE_URL,
    DIGITAL_TWIN_URL: process.env.DIGITAL_TWIN_URL,
    BUILD_TAG: process.env.BUILD_TAG,
    GA_TRACKING: process.env.GA_TRACKING,
    APP_ENV: process.env.APP_ENV,
    WIRED_SCORE_ACCREDITATION: process.env.NEXT_PUBLIC_WIRED_SCORE_ACCREDITATION,
  },
};

// module.exports = withPWA(nextConfig);
module.exports = nextConfig;
